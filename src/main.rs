extern crate piston_window;

use piston_window::*;

struct ResolutionConfig {
    x_max: u32,
    y_max: u32
}



fn main() {
    let resconf = ResolutionConfig { x_max: 640, y_max: 480 };
    let mut window: PistonWindow =
        WindowSettings::new("Hello Piston!", [resconf.x_max, resconf.y_max])
        .exit_on_esc(true)
        .fullscreen(true)
        .vsync(true)
        .build()
        .unwrap();
    let mut x = 0.0_f64;
    let mut speed = 1.0_f64;
    while let Some(event) = window.next() {
        window.draw_2d(&event, |context, graphics| {
            clear([0.0; 4], graphics);
            rectangle([1.0, 0.0, 1.0, 1.0], // rgba
                      [x, 0.0, 10.0, resconf.y_max as f64],
                      context.transform,
                      graphics);
        });
        x = x + speed;
        if x > resconf.x_max as f64 || x < 0.0 {
            speed = -speed;
        }

    }
}
